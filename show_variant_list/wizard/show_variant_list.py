#coding: utf-8
from openerp import models, fields, api, _
from openerp import exceptions
import logging

_logger = logging.getLogger(__name__)


class show_variant_list(models.TransientModel):
    _name = 'show.variant.list'
    _description = 'Show variant list'

    @api.onchange("product_template_id")
    def _onchange_product_template_id(self):
        self.product_ids = self.env["product.product"].search([("product_tmpl_id","=",self.product_template_id.id)])


    product_template_id = fields.Many2one("product.template", string="Product Template", )
    product_ids = fields.Many2many(
        "product.product",
        "product_product_show_variant_list_rel_table",
        "product_product_id",
        "show_variant_list_id",
        string="Product Variants",
    )
    order_line_id = fields.Many2one("sale.order.line", string="Sale Order Line",)