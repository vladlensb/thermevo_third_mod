#coding: utf-8
from openerp import models, fields, api, _
from openerp import exceptions
import logging

_logger = logging.getLogger(__name__)

class sale_order_line(models.Model):
    _inherit = 'sale.order.line'

    @api.multi
    def show_variant_list_wizard(self):
        self.ensure_one()
        context_sp = {
            "default_order_line_id":self.id,
            "default_product_template_id":self.product_id.product_tmpl_id.id,
        }
        res = self.env.ref("show_variant_list.show_variant_list_view_form")
        return {
            'name': _("Variants"),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': res.id,
            'res_model': 'show.variant.list',
            'context': context_sp,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
        }