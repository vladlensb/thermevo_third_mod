#coding: utf-8
from openerp import models, fields, api, _
from openerp import exceptions
import logging

_logger = logging.getLogger(__name__)

class product_product(models.Model):
    _inherit = 'product.product'

    @api.one
    def set_so_position(self):
        if self.env.context.get("order_line_id"):
            order_line_id = self.env["sale.order.line"].browse(self.env.context.get("order_line_id"))
            order_line_id.product_id = self.id
            values = order_line_id.product_id_change(
                order_line_id.order_id.pricelist_id.id,
                order_line_id.product_id.id,
                qty=float(order_line_id.product_uom_qty),
                uom=order_line_id.product_uom.id,
                qty_uos= float(order_line_id.product_uos_qty),
                uos= order_line_id.product_uos.id,
                name=order_line_id.name,
                partner_id=order_line_id.order_id.partner_id.id,
                date_order=order_line_id.order_id.date_order,
                fiscal_position=order_line_id.order_id.fiscal_position.id or False,
                flag=False,
            ).get('value')
            order_line_id.write(values)