# -*- coding: utf-8 -*-
{
    'name': 'Show Sale Variants',
    'version': '1.1',
    'category': 'Sales',
    'summary': 'Access potential alternatives right from a line in a sales order',
    'description': '''
The app is to easily find substitutes for already selected products but with different attributes:
* Click on the button in a sales line to find all possible products of the same template but other attributes values (the button is available both on form and tree view of sale order lines)
* Look through the list and press the button to replace a product selected before with a new one. The list is the same tree shown under the menu Product Variants. It includes description, prices, inventories
* The columns in a sales line would be updated: description, unit of measure, etc.
* The app is fully compatible with warehouse management. Thus, available and virtual stocks would be shown
    ''',
    'price': '50.00',
    'currency': 'EUR',
    'auto_install': False,
    'author': 'IT Libertas',
    'website': 'https://itlibertas.com',
    'depends': [
        "sale",
    ],
    'data': [
        'data/data.xml',
        'security/ir.model.access.csv',
        'views/product_product.xml',
        'wizard/show_variant_list.xml',
        'views/sale_order.xml',
    ],
    'qweb': [

    ],
    'js': [

    ],
    'demo': [

    ],
    'test': [

    ],
    'license': 'AGPL-3',
    'images': [
        'static/description/main.png',
    ],
    'update_xml': [

    ],
    'application': True,
    'installable': True,
    'private_category': False,
    'external_dependencies': {
    },

}
