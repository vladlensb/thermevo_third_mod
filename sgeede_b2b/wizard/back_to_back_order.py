##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from datetime import datetime

from openerp.osv import fields, osv, expression
from openerp.tools.translate import _

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, \
    DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
from openerp.tools.float_utils import float_compare
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)


class BackToBackOrder(osv.osv_memory):
    _name = "back.to.back.order"
    _description = "Back to Back Order"

    def _get_picking_in(self, cr, uid, context=None):
        type_obj = self.pool.get('stock.picking.type')
        user_obj = self.pool.get('res.users')
        company_id = user_obj.browse(
            cr, uid, uid, context=context).company_id.id
        types = type_obj.search(
            cr, uid, [
                ('code', '=', 'incoming'),
                ('warehouse_id.company_id', '=', company_id)],
            context=context)
        if not types:
            types = type_obj.search(
                cr, uid, [('code', '=', 'incoming'),
                          ('warehouse_id', '=', False)], context=context)
            if not types:
                raise osv.except_osv(
                    _('Error!'),
                    _("Make sure you have at least an"
                      " incoming picking type defined"))
        return types[0]

    def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
        partner = self.pool.get('res.partner')
        if not partner_id:
            return {}
        supplier = partner.browse(cr, uid, partner_id, context=context)
        return {'value': {
            'pricelist_id': supplier.property_product_pricelist_purchase.id,
        }}

    _columns = {
        'partner_id': fields.many2one('res.partner',
                                      'Supplier',
                                      required=True),
        'date_order': fields.datetime('Date Order',
                                      required=True),
        'line_ids': fields.one2many('back.to.back.order.line',
                                    'back_order_id',
                                    'Order Lines'),
        'location_id': fields.many2one('stock.location',
                                       'Destination',
                                       required=True,
                                       domain=[('usage', '<>', 'view')]),
        'picking_type_id': fields.many2one(
            'stock.picking.type',
            'Deliver To',
            help="This will determine picking type of incoming shipment",
            required=True),
        'pricelist_id': fields.many2one(
            'product.pricelist',
            'Pricelist',
            help="The pricelist sets the currency used for "
                 "this purchase order. "
                 "It also computes the supplier price for "
                 "the selected products/quantities."),
        'currency_id': fields.many2one('res.currency',
                                       'Currency',
                                       readonly=True),

    }

    _defaults = {
        'date_order': fields.datetime.now,
        'location_id': lambda self, cr, uid, c: self.pool.get(
            'res.users').browse(
            cr, uid, uid, c).company_id.partner_id.property_stock_customer.id,
        'currency_id': lambda self, cr, uid, context: self.pool.get(
            'res.users').browse(
            cr, uid, uid, context=context).company_id.currency_id.id,
        'picking_type_id': _get_picking_in,
    }

    def onchange_pricelist(self, cr, uid, ids, pricelist_id,
                           line_ids, partner_id, context=None):
        product_pricelist = self.pool.get('product.pricelist')
        partner = self.pool.get('res.partner').browse(cr, uid, partner_id,
                                                      context=context)

        if not pricelist_id or not line_ids:
            return {}
        else:
            items = [(6, 0, [])]
            for line in line_ids:
                val = line[2]
                if not val:
                    continue
                else:
                    product = self.pool.get('product.product').browse(
                        cr, uid, int(val['product_id']), context=context)
                    date_order = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

                    date_order_str = datetime.strptime(
                        date_order, DEFAULT_SERVER_DATETIME_FORMAT).strftime(
                        DEFAULT_SERVER_DATE_FORMAT)
                    price = product_pricelist.price_get(
                        cr, uid, [int(pricelist_id)],
                        product.id, val['qty'] or 1.0,
                                    int(partner_id) or False,
                        {'uom': product.uom_po_id.id,
                         'date': date_order_str})[int(pricelist_id)]
                    if price is False:
                        price = product.standard_price

                    line = self.pool.get('sale.order.line').browse(
                        cr, uid, val.get('sale_order_line_id'), context=context)
                    if line:
                        for fixed_product in partner.fixed_product_id:
                            if fixed_product.product_id == line.product_id \
                                    and fixed_product.e_low_open_groove \
                                            == line.e_low_open_groove \
                                    and fixed_product.gluewire == line.gluewire \
                                    and fixed_product.color_laser_marking \
                                            == line.color_laser_marking \
                                    and fixed_product.print_logo == line.print_logo \
                                    and len(
                                        fixed_product.further_processing_ids) \
                                            == len(line.further_processing_ids):
                                check = True
                                for further_processing \
                                        in fixed_product.further_processing_ids:
                                    if further_processing \
                                            not in line.further_processing_ids:
                                        check = False
                                if check:
                                    price = fixed_product.price_unit

                    item = 0, 0, {'product_id': val['product_id'],
                                  'qty': val['qty'],
                                  'name': val['name'],
                                  'price': price or 0.0,
                                  'subtotal': price * val['qty'],
                                  'sale_order_line_id':
                                      val['sale_order_line_id'],
                                  'taxes_ids': val['taxes_ids']}

                    if val['product_id']:
                        items.append(item)
            _logger.info(items)
            return {'value': {'currency_id': self.pool.get(
                'product.pricelist').browse(
                cr, uid, pricelist_id, context=context).currency_id.id,
                              'line_ids': items}}

    def default_get(self, cr, uid, fields, context=None):
        product_pricelist = self.pool.get('product.pricelist')
        if context is None:
            context = {}
        res = super(BackToBackOrder, self).default_get(
            cr, uid, fields, context=context)
        order = self.pool.get('sale.order').browse(
            cr, uid, context['active_id'], context=context)
        items = []
        date_order = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        for line in order.order_line:
            tax_ids = []
            for tax in line.product_id.supplier_taxes_id:
                tax_ids.append(tax.id)

            price = 0.0
            if 'pricelist_id' in res:
                if res['pricelist_id']:
                    date_order_str = datetime.strptime(
                        date_order, DEFAULT_SERVER_DATETIME_FORMAT).strftime(
                        DEFAULT_SERVER_DATE_FORMAT)
                    price = product_pricelist.price_get(
                        cr, uid, [res['pricelist_id']],
                        line.product_id.id, line.price_unit,
                        line.product_uom_qty or 1.0,
                        line.partner_id.id or False,
                        {'uom': line.product_id.uom_po_id.id,
                         'date': date_order_str})[res['pricelist_id']]
                else:
                    price = line.product_id.standard_price
            else:
                price = line.product_id.standard_price

            item = {
                'product_id': line.product_id.id,
                'name': line.name,
                'qty': line.product_uom_qty,
                'price': price,
                'taxes_ids': [(6, 0, tax_ids)],
                'subtotal': line.price_unit * line.product_uom_qty,
                'sale_order_line_id': line.id,
            }
            if line.product_id:
                items.append(item)
        res.update(line_ids=items)
        _logger.info(res)
        return res

    def create_order(self, cr, uid, ids, context):
        _logger.info(self.line_ids)
        purchase_obj = self.pool.get('purchase.order')
        sale_obj = self.pool.get('sale.order')
        purchase_line_obj = self.pool.get('purchase.order.line')
        product_uom = self.pool.get('product.uom')
        product_product = self.pool.get('product.product')
        res_partner = self.pool.get('res.partner')
        supplierinfo = False
        res = {}
        purchase_id = None
        for po in self.browse(cr, uid, ids):
            vals = {
                'partner_id': po.partner_id.id,
                'date_order': po.date_order,
                'picking_type_id': po.picking_type_id.id,
                'location_id': po.location_id.id,
                'invoice_method': 'order',
                'pricelist_id':
                    po.partner_id.property_product_pricelist_purchase
                    and po.partner_id.property_product_pricelist_purchase.id,
                'validator': uid
            }

            purchase_id = purchase_obj.create(cr, uid, vals, context=context)
            context_partner = context.copy()
            if po.partner_id.id:
                lang = res_partner.browse(cr, uid, po.partner_id.id).lang
                context_partner.update(
                    {'lang': lang, 'partner_id': po.partner_id.id})
            for line in po.line_ids:
                if line.qty <= 0:
                    continue
                else:
                    date_order = fields.datetime.now()
                    product = product_product.browse(
                        cr, uid, line.product_id.id, context=context_partner)
                    _logger.info(
                        product_product.name_get(cr, uid, line.product_id.id,
                                                 context=context_partner))
                    name = line.name
                    _logger.info(name)
                    if product.description_purchase:
                        name += '\n' + product.description_purchase
                    precision = self.pool.get(
                        'decimal.precision').precision_get(
                        cr, uid, 'Product Unit of Measure')
                    for supplier in product.seller_ids:
                        if po.partner_id.id \
                                and supplier.name.id == po.partner_id.id:
                            supplierinfo = supplier
                            if (supplierinfo.product_uom.id !=
                                    line.product_uom.id):
                                res['warning'] = {
                                    'title': _('Warning!'),
                                    'message': _('The selected supplier '
                                                 'only sells this product'
                                                 ' by %s')
                                               % supplierinfo.product_uom.name}
                            min_qty = product_uom._compute_qty(
                                cr, uid, supplierinfo.product_uom.id,
                                supplierinfo.min_qty,
                                to_uom_id=line.product_uom.id)
                            if float_compare(min_qty,
                                             line.qty,
                                             precision_digits=precision) == 1:  # If the supplier quantity is greater than entered from user, set minimal.
                                if line.qty:
                                    res['warning'] = {
                                        'title': _('Warning!'),
                                        'message':
                                            _('The selected supplier '
                                              'has a minimal quantity '
                                              'set to %s %s, you should'
                                              ' not purchase less.') %
                                            (supplierinfo.min_qty,
                                             supplierinfo.product_uom.name)}
                                line.qty = min_qty
                    dt = purchase_line_obj._get_date_planned(
                        cr, uid, supplierinfo, date_order,
                        context=context).strftime(
                        DEFAULT_SERVER_DATETIME_FORMAT)
                    tax_ids = []
                    for tax in line.taxes_ids:
                        tax_ids.append(tax.id)

                    purchase_line_pool = self.pool.get('purchase.order.line')
                    values = {
                        'product_id': line.product_id.id,
                        'name': name,
                        'date_planned': dt,
                        'product_qty': line.qty,
                        'price_unit': line.price,
                        'price_subtotal': line.subtotal,
                        'order_id': purchase_id,
                        'sale_order_id': context['active_id'],
                        'taxes_id': [(6, 0, tax_ids)],
                        'product_uom': line.product_id.uom_po_id.id
                    }
                    purchase_line_pool.create(
                        cr, uid, values, context=context)

        sale_order = sale_obj.browse(cr, uid, context.get('active_id'),
                                     context=context)
        po_dict = []
        char_purchase_ids = ''
        for line in sale_order.purchase_line_ids:
            if line.order_id.id not in po_dict:
                char_purchase_ids += '%s, ' % line.order_id.name
        sale_obj.write(
            cr, uid, context.get('active_id'),
            {'char_purchase_ids': char_purchase_ids,
             'purchase_id': purchase_id},
            context=context)

        return True


class BackToBackOrderLine(osv.osv_memory):
    _name = "back.to.back.order.line"
    _description = "Back to Back Order"

    def onchange_product_id(self, cr, uid, ids, product_id, context=None):
        price = self.pool.get("product.product").browse(
            cr, uid, product_id).list_price if product_id else 0
        taxes_id = self.pool.get("product.product").browse(
            cr, uid, product_id).supplier_taxes_id if product_id else []
        tax_ids = []
        for tax in taxes_id:
            tax_ids.append(tax.id)
        return {'value': {'price': price, 'taxes_ids': [(6, 0, tax_ids)]}}

    def onchange_price(self, cr, uid, ids, price, qty, context=None):
        return {'value': {'subtotal': price * qty}}

    def _amount_line(self, cr, uid, ids, prop, arg, context=None):
        res = {}
        cur_obj = self.pool.get('res.currency')
        tax_obj = self.pool.get('account.tax')
        for line in self.browse(cr, uid, ids, context=context):
            taxes = tax_obj.compute_all(
                cr, uid, line.taxes_ids, line.price, line.qty, line.product_id,
                line.back_order_id.partner_id)
            cur = line.back_order_id.pricelist_id.currency_id
            res[line.id] = cur_obj.round(cr, uid, cur, taxes['total'])
        return res

    def _get_uom_id(self, cr, uid, context=None):
        try:
            proxy = self.pool.get('ir.model.data')
            result = proxy.get_object_reference(
                cr, uid, 'product', 'product_uom_unit')
            return result[1]
        except Exception, ex:
            return False

    _columns = {

        'product_id': fields.many2one('product.product',
                                      'Product'),
        'name': fields.char('Description'),
        'back_order_id': fields.many2one('back.to.back.order',
                                         'Back Order'),
        'qty': fields.float('Quantity'),
        'price': fields.float('Unit Price'),
        'subtotal': fields.function(
            _amount_line,
            string='Subtotal',
            digits_compute=dp.get_precision('Account')),
        'product_uom': fields.many2one('product.uom',
                                       'Product Unit of Measure',
                                       required=True),
        'taxes_ids': fields.many2many('account.tax',
                                      'b_to_b_purchase_order_taxes',
                                      'line_id',
                                      'tax_id',
                                      string='Taxes',
                                      required=True),
        'sale_order_line_id': fields.many2one('sale.order.line',
                                              string="Sale Order line")

    }

    _defaults = {
        'product_uom': _get_uom_id,
    }

    # vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
