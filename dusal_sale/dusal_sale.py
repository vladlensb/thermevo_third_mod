# -*- coding: utf-8 -*-
##############################################################################
#
#    Addons by Dusal.net
#    Copyright (C) 2016 Dusal.net Almas
#
#    Odoo Proprietary License v1.0
#    
#    This software and associated files (the "Software") may only be used (executed,
#    modified, executed after modifications) if you have purchased a valid license
#    from the authors, typically via Odoo Apps, or if you have received a written
#    agreement from the authors of the Software (see the COPYRIGHT file).
#
#    You may develop Odoo modules that use the Software as a library (typically
#    by depending on it, importing it and using its resources), but without copying
#    any source code or material from the Software. You may distribute those
#    modules under the license of your choice, provided that this license is
#    compatible with the terms of the Odoo Proprietary License (For example:
#    LGPL, MIT, or proprietary licenses similar to this one).
#    
#    It is forbidden to publish, distribute, sublicense, or sell copies of the Software
#    or modified copies of the Software.
#    
#    The above copyright notice and this permission notice must be included in all
#    copies or substantial portions of the Software.
#    
#    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
#    DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
#    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#    DEALINGS IN THE SOFTWARE.
#
##############################################################################


import logging

from openerp import SUPERUSER_ID
from openerp import tools
from openerp.modules.module import get_module_resource
from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime
from openerp import models


class sale_order(models.Model):
    _name = 'sale.order'
    _inherit = 'sale.order'
    
    _columns = {
        'print_product_image':fields.boolean('Print product image', readonly=False, select=True, help="If this checkbox checked then print product images on Sales order & Quotation"),
        'image_size': fields.selection([('small', 'Small'), ('medium', 'Medium'), ('original', 'Big')], 'Image sizes', help="Choose an image size here", select=True),
        'print_line_number':fields.boolean('Print line number', readonly=False, select=True, help="Print line number on Sales order & Quotation"),
    }
    _defaults = {   'print_product_image': True, 
                    'image_size': 'small',
                    'print_line_number': False, 
                    }
    
class sale_order_line(models.Model):
    _name = 'sale.order.line'
    _inherit = 'sale.order.line'
    
    _columns = {
        'product_image_small': fields.related('product_id', 'image_small', type='binary', string='Image small'),
        'product_image_medium': fields.related('product_id', 'image_medium', type='binary', string='Image medium'),
        'product_image': fields.related('product_id', 'image', type='binary', string='Image'),
    }
