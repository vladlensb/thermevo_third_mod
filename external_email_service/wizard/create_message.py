# -*- coding: utf-8 -*-
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import fields, models, api
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class CreateMessageWizard(models.TransientModel):
    _name = 'create.message.wizard'
    _description = 'Create Message'

    u_id = fields.Many2one('res.users', string='User')
    mail_from = fields.Char(string='Mail From')
    mail_to = fields.Char(string='Mail To')
    mail_reply_to = fields.Char(string='Reply To')
    subject = fields.Char(string='Subject')
    body = fields.Html(string='Body')
    external_parent_id = fields.Many2one('external.mail.messages',
                                         srting='Parent ID')
    attachment_ids = fields.Many2many('ir.attachment',
                                      'external_message_attachment_wizard_rel',
                                      'message_id',
                                      'attachment_id',
                                      string='Attachments')

    @api.multi
    def send_message(self):
        external_mail_message_pool = self.env['external.mail.messages']
        if self.external_parent_id:
            external_values = {
                'user_id': self.external_parent_id.user_id,
                'partner_id': self.external_parent_id.partner_id,
                'lead_id': self.external_parent_id.lead_id,
                'delivered_to': self.external_parent_id.delivered_to,
                'mail_from': self.mail_from,
                'mail_to': self.mail_to,
                'mail_reply_to': self.mail_reply_to,
                'date': datetime.now(),
                'message_id': '',
                'subject': self.subject,
                'body': self.body,
                'external_parent_id': self.external_parent_id.id,
                'type': 'outgoing',
                'is_read': True,
                'attachment_ids': self.attachment_ids,
            }
            message = external_mail_message_pool.create(external_values)
            message.write({'u_id': self.u_id.id})
            template_name = 'Private message'
            mail_notification(self, template_name)

        else:
            values = {
                'mail_from': self.mail_to,
                'mail_to': self.mail_from,
                'mail_reply_to': self.mail_reply_to,
                'date': datetime.now(),
                'message_id': '',
                'subject': self.subject,
                'body': self.body,
                'type': 'outgoing',
                'is_read': True,
                'attachment_ids': self.attachment_ids,
            }
            message = external_mail_message_pool.create(values)
            message.write({'u_id': self.u_id.id})
            template_name = 'Private message'
            mail_notification(self, template_name)


@api.multi
def mail_notification(obj, template_name):
    email_template_pool = obj.env['email.template']
    template_id = email_template_pool.search([('name', '=', template_name)])
    mail_id = obj.pool['email.template'].send_mail(
        obj.env.cr, obj.env.uid, template_id.id, obj.id, force_send=False,
        context=obj.env.context)
    mail = obj.env['mail.mail'].search([('id', '=', mail_id)])
    if obj.attachment_ids:
        att_ids = []
        for a in obj.attachment_ids:
            att_ids.append(a.id)
        mail.write({'attachment_ids': [(6, 0, att_ids)]})
    obj.env['mail.mail'].send(mail)
    _logger.debug('Create a new message id = %s', mail_id)
    return mail_id
