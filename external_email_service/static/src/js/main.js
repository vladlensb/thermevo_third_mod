openerp.external_email_service = function(instance, local){

    var Dialogue_emails = new openerp.Model('external.mail.dialogue');
    var Emails = new openerp.Model('external.mail.messages');
    var Template_answer = new openerp.Model('email.template');

    instance.web.form.widgets.add('dialogue_widget', 'openerp.external_email_service.dialogue_widget');
    instance.external_email_service.dialogue_widget = openerp.web.form.FieldChar.extend(
        {
            template: "dialogue_widget",
            emails: [],


            init: function (view, code) {
                this._super(view, code);
            },

            start: function () {
                this._super();
                var self = this,
                    $body = $('#dialogue_widget'),
                    $emails_list = $('#emails_list'),
                    dialogue_id = this.format_value(this.get('value'), '');

                var email_template = '{{#emails}}<div class="email {{type}}">' +
                        '<div class="email_header {{^is_read}}unread{{/is_read}}" data-id="{{id}}">' +
                            '<span class="email_subject">{{subject}}</span>' +
                            '<span class="email_date">{{date}}</span>' +
                        '</div>' +
                        '<div class="email_body">{{{body}}}</div>' +
                    '</div>{{/emails}}';

                this.$el.find('input').val(dialogue_id);

                Dialogue_emails.call('get_dialogue', [dialogue_id]).then(function(result){
                    console.log(result)
                    self.dialogue = result[0];
                    self.emails = result[0]['emails'];

                    $emails_list.html(Mustache.render(email_template, {emails: self.emails}));
                });


                $emails_list.on('click', '.email_header', function () {
                    if ($(this).hasClass('unread')){
                        var email_id = $(this).data('id');
                        Emails.call('message_is_read', [email_id]).then(function(result){});
                        $(this).removeClass('unread');
                    }
                    $(this).parent('.email').find('.email_body').slideToggle(300);

                });

                // Write answer
                $body.on('click','.write_answer', function (e) {
                    e.preventDefault();
                    var $this = $(this);

                    Dialogue_emails
                        .call('write_answer', [dialogue_id], {context: new instance.web.CompoundContext()})
                        .then(function(response){
                            self.do_action(response[0]);
                        });
                });

                // Template answer
                $body.on('click','.template_answer', function (e) {
                    e.preventDefault();
                    var $this = $(this);

                    Template_answer
                        .call('get_template_answer_form', [dialogue_id], {context: new instance.web.CompoundContext()})
                        .then(function(response){
                            self.do_action(response[0]);
                        });
                });
            }
        }
    );
};
