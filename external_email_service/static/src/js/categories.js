openerp.category_widget = function (instance) {

    var UserCategories = new openerp.Model('external.mail.category');

    instance.web.client_actions.add('local.CategoriesWidget', 'instance.category_widget.CategoriesWidget');
    local.CategoriesWidget = instance.Widget.extend({
        start: function() {
            this.$el.append(QWeb.render("user_categories"));

            UserCategories.call('get_user_categories').then(function(result){
                    console.log(result);

                    // $emails_list.html(Mustache.render(email_template, {emails: self.emails}));
            });
        },
    });
}