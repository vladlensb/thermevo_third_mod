# -*- coding: utf-8 -*-
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from datetime import datetime

from openerp import models, api

from ..wizard.create_message import mail_notification


class TemplateAnswer(models.Model):
    _inherit = 'email.template'

    @api.multi
    def template_answer(self):
        external_mail_message_pool = self.env['external.mail.messages']

        template_id = self._context['template_ids']
        template = self.env['email.template'].search([
            ('id', '=', template_id)])
        values = {
            'mail_from': self.mail_to,
            'mail_to': self.mail_from,
            'mail_reply_to': self.mail_reply_to,
            'date': datetime.now(),
            'message_id': '',
            'subject': template.subject,
            'type': 'outgoing',
            'is_read': True,
            'attachment_ids': self.attachment_ids,
        }
        message = external_mail_message_pool.create(values)
        message.write({'u_id': self.u_id.id})
        mail_notification(self, template.name)

    @api.one
    def get_template_answer_form(self):
        view_id = self.env.ref(
            'external_email_service.external_mail_template_answer_form').id
        dialogue = self.env['external.mail.dialogue'].search([
            ('id', '=', self.id)
        ])

        context = {
            'email_from': dialogue.email_ids[0].mail_to,
            'email_to': dialogue.email_ids[0].mail_from,
            'type': 'outgoing',
            'auto_delete': False,
            'dialogue_id': dialogue.id
        }

        return {
            'views': [(view_id, 'kanban')],
            'view_id': view_id,
            'type': 'ir.actions.act_window',
            'context': context,
            'res_model': 'email.template',
            'domain': [('model', '=', 'external.mail.dialogue')],
            'target': 'new'
        }

    def generate_email_batch(self, *args, **kwargs):
        result = super(TemplateAnswer, self).generate_email_batch(*args, **kwargs)
        context = kwargs.get('context', {})
        email_to = context.get('email_to', '')
        email_from = context.get('email_from', '')
        for key in result:
            if email_to:
                result[key]['email_to'] = email_to
            if email_from:
                result[key]['email_from'] = email_from
            if 'auto_delete' in context:
                result[key]['auto_delete'] = context['auto_delete']
        return result

    # @api.one
    # def send_mail(self, *args, **kwargs):
    #     res_id = self._context['dialogue_id']
    #
    #     message_id = super(TemplateAnswer, self).send_mail(
    #         res_id=res_id, context=self._context, force_send=True)
    #
    #     message = self.env['mail.mail'].browse(message_id)
    #     attachment_ids = [attachment.id for attachment in message['attachment_ids']]
    #
    #     # Converting model to dictionary and create dialogue message.
    #     message_as_dict = message.read()[0]
    #     message_as_dict.update({
    #         'mail_from': message_as_dict['email_from'],
    #         'mail_to': message_as_dict['email_to'],
    #         'state': 'all',
    #         'attachment_ids': [(6, 0, attachment_ids)],
    #         'dialogue_id': res_id,
    #         'type': 'outgoing'
    #     })
    #     self.env['external.mail.messages'].create(message_as_dict)
    #
    #     # Removing manually email message because of disabled auto deleting
    #     # after sending.
    #     if not self._context.get('auto_delete', True):
    #         message.unlink()
    #     return message_id

