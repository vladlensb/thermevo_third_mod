# -*- coding: utf-8 -*-
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from email.utils import parseaddr

from openerp import fields, models, api
import logging

_logger = logging.getLogger(__name__)


class ExternalMailMessage(models.Model):
    _name = 'external.mail.messages'
    _rec_name = 'subject'
    _order = "date desc"
    _inherit = ['ir.needaction_mixin']

    u_id = fields.Many2one('res.users', string='User')
    user_id = fields.Many2one('res.users', string='User')
    partner_id = fields.Many2one('res.partner', string='Partner')
    lead_id = fields.Many2one('crm.lead', string='Lead')
    delivered_to = fields.Char(string='Delivered-To')
    mail_to = fields.Char(string='Mail To')
    mail_from = fields.Char(string='From')
    mail_reply_to = fields.Char(string='Mail Reply To')
    date = fields.Datetime(string='Date')
    message_id = fields.Char(string='Message ID')
    subject = fields.Char(string='Subject')
    body = fields.Html(string='Body')
    dialogue_id = fields.Many2one('external.mail.dialogue', string='Dialogue')
    external_parent_id = fields.Many2one('external.mail.messages',
                                         srting='Parent ID')
    type = fields.Selection([('incoming', 'incoming'),
                             ('outgoing', 'outgoing')],
                            string='Type')
    state = fields.Selection([('personal', 'Personal'), ('all', 'All')],
                             string='owner')
    notification = fields.Boolean()
    assigned_to = fields.Many2one('res.users',
                                  string='Assigned To')
    is_read = fields.Boolean(string='State')
    thread_references = fields.Char(string='Thread Rreferences')
    attachment_ids = fields.Many2many('ir.attachment',
                                      'external_message_attachment_rel',
                                      'message_id',
                                      'attachment_id',
                                      string='Attachments')

    @api.model
    def _needaction_domain_get(self):
        if self.env.context.get('count_menu') == 'alle_einnahmen':
            return [('state', '=', 'all')]
        if self.env.context.get('count_menu') == 'notification':
            return [('notification', '=', True)]

        return False

    @api.multi
    def download_attachment(self, attachment_id):
        """ Return the content of linked attachments. """
        message_values = self.read(['attachment_ids'])[0]
        if attachment_id in message_values['attachment_ids']:
            attachment = self.pool.get('ir.attachment').browse(attachment_id)
            if attachment.datas and attachment.datas_fname:
                return {
                    'base64': attachment.datas,
                    'filename': attachment.datas_fname,
                }
        return False

    @api.model
    def create(self, values):
        servers = []
        titles = []
        res_user_pool = self.env['res.users']
        mail_servers_model = self.env['fetchmail.server']

        # collect all incoming email server addresses
        for server in mail_servers_model.search([('state', '=', 'done')]):
            servers.append(server.user)

        # parsing emails
        mail_from = parseaddr(values['mail_from'])[1].lower()
        mail_to = parseaddr(values['mail_to'])[1].lower()
        lead_name = values['mail_from']

        values['mail_from'] = mail_from
        values['mail_to'] = mail_to

        # check if user assigned to employee
        previous_email = self.search([('id', '!=', self.id),
                                      ('mail_from', '=', mail_from)], limit=1)
        ''' if partners's emails assigned to employee assign new email from
            partner to this employee '''
        if previous_email.assigned_to:
            values['assigned_to'] = previous_email.assigned_to.id
            values['dialogue_id'] = previous_email.dialogue_id.id
            values['state'] = 'personal'
            previous_email.assigned_to.post_notification(
                title='New email',
                message='You have new email'
            )
        else:
            values['state'] = 'all'

        # checking for user exists by email in res.users
        user_from = res_user_pool.search([('login', '=', mail_from)])
        # checking for partner exists by email in res.partners
        partner = self.env['res.partner'].search([('email', '=', 'mail_from')],
                                                 limit=1)
        if user_from:
            values['user_id'] = user_from.id
        elif partner:
            values['partner_id'] = partner.id

        if not values.get('type'):
            values['type'] = 'incoming'

        if values['type'] == 'outgoing':
            values['is_read'] = True

        subject = values.get('subject')
        if mail_from.split(' ')[-1] in servers:
            values['notification'] = True
        if subject in titles:
            values['notification'] = True

        attachments = values.get('attachments')
        if attachments:
            ir_attachment_pool = self.env['ir.attachment']
            attachment_ids = []
            for a in attachments:
                vals = {
                    'name': a[0],
                    'db_datas': a[1],
                    'datas': a[1],
                    'res_model': 'external.mail.messages',
                    'res_name': a[0],
                    'datas_fname': a[0],
                }
                try:
                    attachment_id = ir_attachment_pool.create(vals)
                    attachment_ids.append(attachment_id.id)
                except:
                    pass

            values['attachment_ids'] = [(6, 0, attachment_ids)]
        return super(ExternalMailMessage, self).create(values)

    @api.one
    def message_is_read(self):
        self.is_read = True
        return {'success': True, 'is_read': self.is_read}

    @api.one
    def assigned_to_me(self):
        """
        Method for assign customer to employee by pressing "Assign_to_me"
        button in email detail view
        :return: None
        """
        dialogue = self.env['external.mail.dialogue']
        if not self.assigned_to:
            self.assigned_to = self.env.user.id
            self.state = 'personal'

            if self.lead_id:
                user_name = self.lead_id.name
            else:
                user_name = self.mail_from

            d = dialogue.create({
                'assigned_to': self.assigned_to.id,
                'user_name': user_name,
                'mail_from': self.mail_from
            })
            self.dialogue_id = d.id

    @api.multi
    def reply(self):

        context = {
            'default_mail_to': self.mail_from,
            'default_u_id': self.env.user.id,
            'default_mail_from': self.mail_to,
            'default_subject': self.subject,
            'default_external_parent_id': self.id,
        }

        return {
            'type': 'ir.actions.act_window',
            'res_model': 'create.message.wizard',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'target': 'new',
            'context': context
        }

    @api.model
    def check_for_notification(self):
        # TODO: Implement in future
        pass

    @api.model
    def check_for_assigned_to(self):
        # TODO: Implement in future
        pass


class IrAttachment(models.Model):
    _inherit = "ir.attachment"

    @api.multi
    def check_message(self):
        attachments = self.search(
            [('res_model', '=', 'external.mail.messages')])
        for attachment in attachments:
            attachment.write({'datas_fname': attachment.name})
