# -*- coding: utf-8 -*-
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You shouexternal.mail.notification_titleld have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################


from email.message import Message
from email.utils import parseaddr
from openerp.addons.mail.mail_message import decode
from openerp.osv import fields, orm, osv
import logging

_logger = logging.getLogger(__name__)


def decode_header(message, header, separator=' '):
    return separator.join(
        map(decode, filter(None, message.get_all(header, []))))


class MailThread(orm.AbstractModel):
    _inherit = 'mail.thread'

    def message_route(self, cr, uid, message, message_dict, model=None,
                      thread_id=None, custom_values=None, context=None):

        if not isinstance(message, Message):
            raise TypeError(
                'message must be an email.message.Message at this point')
        subject = decode_header(message, 'Subject')
        _logger.info(subject)
        references = decode_header(message, 'References')
        in_reply_to = decode_header(message, 'In-Reply-To')
        thread_references = references or in_reply_to
        to = decode_header(message, 'To')
        mail_from = decode_header(message, 'From')
        subject_dict = subject.split(' ')
        mail_message_pool = self.pool.get('mail.message')
        for subject_element in subject_dict:
            mail_message_ids = mail_message_pool.search(
                cr, uid, [('record_name', '=', subject_element)])
            mail_message = mail_message_pool.browse(
                cr, uid, mail_message_ids, context=context)
            try:
                mail_message = mail_message[-1]
            except:
                pass
            if mail_message:
                message_dict['parent_id'] = mail_message.id
                model = mail_message.model
                thread_id = mail_message.res_id
        if not message_dict.get('parent_id') \
                and message_dict.get('mail_from'):
            mail_from = parseaddr(message_dict['mail_from'])[1].lower()
            partner = self.pool.get('res.partner').search(
                cr, uid, [('email', '=', mail_from)], limit=1)
            if partner:
                mail_message_ids = mail_message_pool.search(
                    cr, uid, [('res_id', '=', partner.id),
                              ('model', '=', 'res.partner')])
                mail_message = mail_message_pool.browse(
                    cr, uid, mail_message_ids, context=context)
                try:
                    mail_message = mail_message[0]
                except:
                    pass
                if mail_message:
                    message_dict['parent_id'] = mail_message.id
                    model = mail_message.model
                    thread_id = mail_message.res_id
        if not message_dict.get('parent_id'):
            message_dict['subject'] = subject
            message_dict['references'] = references
            message_dict['mail_reply_to'] = in_reply_to
            message_dict['thread_references'] = thread_references
            message_dict['mail_to'] = to
            message_dict['mail_from'] = mail_from
            message_dict['type'] = 'incoming'
            message_dict['is_new'] = False
            mail_from = parseaddr(message_dict['mail_from'])[1].lower()

            partner = self.pool.get('res.partner').search(
                cr, uid, [('email', '=', mail_from)], limit=1)

            if partner:
                self.pool.get('external.mail.messages').create(
                    cr, uid, message_dict, context=context)
        else:
            route = super(MailThread, self).message_route(
                cr, uid, message, message_dict, model=model,
                thread_id=thread_id, custom_values=custom_values,
                context=context)
            return route
